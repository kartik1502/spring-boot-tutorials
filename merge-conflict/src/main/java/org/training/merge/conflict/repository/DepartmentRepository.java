package org.training.merge.conflict.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.merge.conflict.entity.Department;

public interface DepartmentRepository extends JpaRepository<Department, Long> {

}
