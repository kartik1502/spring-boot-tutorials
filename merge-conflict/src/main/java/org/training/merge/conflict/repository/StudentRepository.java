package org.training.merge.conflict.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.merge.conflict.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Long>{

}