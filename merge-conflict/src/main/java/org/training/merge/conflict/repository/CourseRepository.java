package org.training.merge.conflict.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.merge.conflict.entity.Course;

public interface CourseRepository extends JpaRepository<Course, Long> {

}