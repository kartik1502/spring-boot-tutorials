package org.training.kafka.service.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.training.kafka.service.configuration.KafkaProducer;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/kafka")
@RequiredArgsConstructor
public class MessageController {

	private final KafkaProducer kafkaProducer;
	
	
	@GetMapping("/publish")
	public ResponseEntity<String> publish(@RequestParam String message) {
		kafkaProducer.sendMessage(message);
		return ResponseEntity.ok().body("Message sent to kafka");
	}
}
