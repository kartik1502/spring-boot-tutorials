package org.training.kafka.service.configuration;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.training.kafka.service.constants.GlobalConstants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class KafkaConsumer {

	@KafkaListener(topics = GlobalConstants.TOPIC_NAME, 
			groupId = GlobalConstants.GROUP_ID)
	public void comsume(String message) {
		log.info(String.format("Message received: %s", message));
	}
	
}
