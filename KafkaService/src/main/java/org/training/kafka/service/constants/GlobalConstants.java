package org.training.kafka.service.constants;

public class GlobalConstants {
	
	public static final String TOPIC_NAME = "kafka-spring-boot";
	
	public static final String GROUP_ID = "group-id";

}
