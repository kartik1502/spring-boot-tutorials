package org.training.kafka.service.configuration;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.training.kafka.service.constants.GlobalConstants;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class KafkaProducer {

	private final KafkaTemplate<String, String> kafkaTemplate;
	
	public void sendMessage(String message) {
		log.info(String.format("Message sent: %s", message));
		kafkaTemplate.send(GlobalConstants.TOPIC_NAME, message);
	}
	
}
