package org.training.inheritance.mapping.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class BeneficiaryAccount extends Account{

	private String branchName;
	
	private String accountHolderName;
	
	@ManyToOne
	@JoinColumn(name = "customerId")
	private Customer customer;
}