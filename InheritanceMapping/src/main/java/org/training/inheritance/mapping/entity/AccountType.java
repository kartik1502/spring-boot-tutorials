package org.training.inheritance.mapping.entity;

public enum AccountType {

	SAVINGS, CURRENT, LOAN
}
