package org.training.inheritance.mapping.exception;

public class UnauthorizedAccess extends GlobalException{

	private static final long serialVersionUID = 1L;

	public UnauthorizedAccess(String message) {
		super(message, GlobalErrorCode.UN_AUTHORIZED);
	}
	
	public UnauthorizedAccess() {
		super("Restricted Access", GlobalErrorCode.UN_AUTHORIZED);
	}
	
}
