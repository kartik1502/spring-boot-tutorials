package org.training.inheritance.mapping.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.training.inheritance.mapping.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	Optional<Customer> findByEmailIdOrContactNo(String emailId, String contactNo);
	
	@Query("select count(*) from Customer")
	long count();
	
	Optional<Customer> findByCustomerId(String customerId);

}
