package org.training.inheritance.mapping.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.inheritance.mapping.entity.BankAccount;
import org.training.inheritance.mapping.entity.Customer;

public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {
	
	Optional<BankAccount> findByCustomer(Customer customer);

}
