package org.training.inheritance.mapping.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AnotherResponse {

	private String errorCode;

	private String errorMessage;

}
