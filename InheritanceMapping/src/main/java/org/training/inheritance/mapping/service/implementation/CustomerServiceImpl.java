package org.training.inheritance.mapping.service.implementation;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.training.inheritance.mapping.dto.Register;
import org.training.inheritance.mapping.dto.Response;
import org.training.inheritance.mapping.entity.Customer;
import org.training.inheritance.mapping.entity.LoginStatus;
import org.training.inheritance.mapping.exception.PasswordMissmatch;
import org.training.inheritance.mapping.exception.ResourceConflict;
import org.training.inheritance.mapping.exception.ResourceNotFound;
import org.training.inheritance.mapping.exception.UnauthorizedAccess;
import org.training.inheritance.mapping.repository.CustomerRepository;
import org.training.inheritance.mapping.service.CustomerService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService{

	private final CustomerRepository customerRepository;
	
	@Value("${spring.application.ok}")
	private String responseOk;
	
	
	@Override
	public Response register(Register register) {
		
		if(!register.password().equals(register.confirmPassword())) {
			log.warn("Password and confirm password miss match");
			throw new PasswordMissmatch("Password and confirm password needs to match");
		}
		
		customerRepository.findByEmailIdOrContactNo(register.emailId(), register.contactNo())
				.ifPresent(existingCustomer -> {
					log.warn("Customer with same emailId or the contact number is already present");
					throw new ResourceConflict("Customer already exists");
				});
		
		Customer newCustomer = new Customer();
		BeanUtils.copyProperties(register, newCustomer);
		newCustomer.setCustomerId(String.format("%s%06d", newCustomer.getName().substring(0, 4).toUpperCase(), customerRepository.count()));
		newCustomer.setLoginStatus(LoginStatus.LOGOUT);
		customerRepository.save(newCustomer);
		return new Response(responseOk, "Customer registerd Successfully");
	}

	@Override
	public Response login(String customerId, String password) {
		
		Customer customer = customerRepository.findByCustomerId(customerId)
				.orElseThrow(() -> {
					log.warn("Requested customer not found");
					return new ResourceNotFound("Requested Customer not found");
				});
		
		if(!customer.getPassword().equals(password)) {
			throw new UnauthorizedAccess("Invalid Credentials");
		}
		
		customer.setLoginStatus(LoginStatus.LOGIN);
		customerRepository.save(customer);
		return new Response(responseOk, "Login Successfull");
	}

	@Override
	public Response logout(String customerId) {
		
		Customer customer = customerRepository.findByCustomerId(customerId)
				.orElseThrow(() -> {
					log.warn("Requested customer not found");
					return new ResourceNotFound("Requested Customer not found");
				});
		
		customer.setLoginStatus(LoginStatus.LOGOUT);
		customerRepository.save(customer);
		return new Response(responseOk, "Logout Successfull");
	}

}
