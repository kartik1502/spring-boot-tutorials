package org.training.inheritance.mapping.exception;

public class PasswordMissmatch extends GlobalException{

	private static final long serialVersionUID = 1L;

	public PasswordMissmatch(String message) {
		super(message, GlobalErrorCode.BAD_REQUEST);
	}
	
	public PasswordMissmatch() {
		super("Password dose not match", GlobalErrorCode.BAD_REQUEST);
	}
}
