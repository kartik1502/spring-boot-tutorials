package org.training.inheritance.mapping.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;

public record Register(
		
		@Pattern(regexp = "[a-zA-Z ]+", message = "Name should contain only aplhabets") String name,
		@Pattern(regexp = "\\d{12}", message = "Aadhar number should contain exactly 12 digits") String aadhar,
		@Email(message = "please enter a valid email") String emailId,
		@Pattern(regexp = "[6-9]\\d{9}", message = "please enter a valid contact number") String contactNo,
		@Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$", 
		message = "Password should have atleast 8 characters, one uppercase, one lowercase alphabet, a number and one special character") String password,
		@Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$", 
		message = "Password should have atleast 8 characters, one uppercase, one lowercase alphabet, a number and one special character") String confirmPassword) {

}
