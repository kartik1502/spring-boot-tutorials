package org.training.inheritance.mapping.exception;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler{
	
	@Value("${spring.application.bad_request}")
	private String badRequest;
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {

		List<String> errors = ex.getBindingResult().getAllErrors().stream().map(error -> error.getDefaultMessage()).toList();
		
		return new ResponseEntity<>(new ErrorResponse(badRequest, errors), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(GlobalException.class)
	public ResponseEntity<Object> handleGlobalException(GlobalException globalException) {
		return ResponseEntity
				.status(HttpStatus.valueOf(Integer.valueOf(globalException.getErrorCode())))
				.body(AnotherResponse.builder()
						.errorCode(globalException.getErrorCode())
						.errorMessage(globalException.getMessage()).build());
	}

}
