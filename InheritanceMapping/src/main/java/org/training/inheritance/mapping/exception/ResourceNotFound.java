package org.training.inheritance.mapping.exception;

public class ResourceNotFound extends GlobalException{

	private static final long serialVersionUID = 1L;

	public ResourceNotFound(String message) {
		super(message, GlobalErrorCode.NOT_FOUND);
	}
	
	public ResourceNotFound() {
		super("Requested resource not found", GlobalErrorCode.NOT_FOUND);
	}

}
