package org.training.inheritance.mapping.dto;

public record Response(String responseCode, String responseMessage) {

}
