package org.training.inheritance.mapping.service.implementation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.training.inheritance.mapping.dto.CreateAccount;
import org.training.inheritance.mapping.dto.Response;
import org.training.inheritance.mapping.dto.ViewAccount;
import org.training.inheritance.mapping.dto.ViewCustomer;
import org.training.inheritance.mapping.entity.AccountType;
import org.training.inheritance.mapping.entity.BankAccount;
import org.training.inheritance.mapping.entity.Customer;
import org.training.inheritance.mapping.entity.IfscCode;
import org.training.inheritance.mapping.entity.LoginStatus;
import org.training.inheritance.mapping.exception.ResourceNotFound;
import org.training.inheritance.mapping.exception.UnauthorizedAccess;
import org.training.inheritance.mapping.repository.BankAccountRepository;
import org.training.inheritance.mapping.repository.CustomerRepository;
import org.training.inheritance.mapping.service.AccountService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService{
	
	private final BankAccountRepository bankAccountRepository;
	
	private final CustomerRepository customerRepository;
	
	@Value("${spring.application.ok}")
	private String responseOk;
	
	@Override
	public Response addBankAccount(@Valid CreateAccount bankAccountDto) {
		
		Customer customer = customerRepository.findByCustomerId(bankAccountDto.customerId())
				.orElseThrow(() -> {
					log.warn("rquested customer not found");
					return new ResourceNotFound("Reqested customer not found");
				});
		if(customer.getLoginStatus().equals(LoginStatus.LOGOUT)) {
			log.warn("Access denied, login to create the bank account");
			throw new UnauthorizedAccess("Access denied, need to login");
		}
		BankAccount bankAccount = new BankAccount(bankAccountDto.balance(), customer);
		bankAccount.setAccountNumber(String.format("%s%06d", IfscCode.UTIB009.toString(), bankAccountRepository.count()));
		bankAccount.setAccountType(AccountType.valueOf(bankAccountDto.accountType()));
		bankAccountRepository.save(bankAccount);
		
		return new Response(responseOk, "Bank Account created");
				
	}

	@Override
	public ViewAccount getAccount(String customerId) {
		
		Customer customer = customerRepository.findByCustomerId(customerId)
				.orElseThrow(() -> {
					log.warn("Requested customer not found");
					return new ResourceNotFound("resgister to fetch account details");
				});
		
		BankAccount bankAccount = bankAccountRepository.findByCustomer(customer)
				.orElseThrow(() -> {
					log.warn("Requested account not found");
					return new ResourceNotFound("Customer's account not generated yet");
				});
		
		return new ViewAccount(bankAccount.getAccountNumber(), 
				bankAccount.getAccountType().toString(), 
				bankAccount.getBalance(), new ViewCustomer(customer.getCustomerId(), customer.getName(), customer.getAadhar(), customer.getEmailId(), customer.getContactNo()));
	}
	
	

}