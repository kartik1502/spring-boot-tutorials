package org.training.inheritance.mapping.dto;

public record ViewCustomer(String customerId, String name, String aadhar, String emailId,
		String contactNo) {

}
