package org.training.inheritance.mapping.exception;

public class ResourceConflict extends GlobalException{

	private static final long serialVersionUID = 1L;	

	public ResourceConflict(String message) {
		super(message, GlobalErrorCode.CONFLICT);
	}
	
	public ResourceConflict() {
		super("requested resource not found", GlobalErrorCode.CONFLICT);
	}
}
