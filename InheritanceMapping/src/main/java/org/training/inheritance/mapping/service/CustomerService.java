package org.training.inheritance.mapping.service;

import org.training.inheritance.mapping.dto.Register;
import org.training.inheritance.mapping.dto.Response;

import jakarta.validation.Valid;

public interface CustomerService {

	Response register(@Valid Register register);

	Response login(String customerId, String password);

	Response logout(String customerId);

}
