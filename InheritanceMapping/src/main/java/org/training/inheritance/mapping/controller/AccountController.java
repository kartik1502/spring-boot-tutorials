package org.training.inheritance.mapping.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.training.inheritance.mapping.dto.CreateAccount;
import org.training.inheritance.mapping.dto.Response;
import org.training.inheritance.mapping.dto.ViewAccount;
import org.training.inheritance.mapping.service.AccountService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
public class AccountController {

	private final AccountService accountService;
	
	@PostMapping
	public ResponseEntity<Response> addBankAccount(@RequestBody @Valid CreateAccount bankAccountDto){
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(accountService.addBankAccount(bankAccountDto));
	}
	
	@GetMapping
	public ResponseEntity<ViewAccount> getAccount(@RequestParam String customerId) {
		return ResponseEntity.ok()
				.body(accountService.getAccount(customerId));
	}
}
