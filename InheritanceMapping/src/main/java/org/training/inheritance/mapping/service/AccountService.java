package org.training.inheritance.mapping.service;

import org.training.inheritance.mapping.dto.CreateAccount;
import org.training.inheritance.mapping.dto.Response;
import org.training.inheritance.mapping.dto.ViewAccount;

import jakarta.validation.Valid;

public interface AccountService {

	Response addBankAccount(@Valid CreateAccount bankAccountDto);

	ViewAccount getAccount(String customerId);
	
}
