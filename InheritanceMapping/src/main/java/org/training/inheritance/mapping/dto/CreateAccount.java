package org.training.inheritance.mapping.dto;

import java.math.BigDecimal;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;

public record CreateAccount(@Pattern(regexp = "(SAVINGS)|(CURRENT)|(LOAN)" , message = "Enter a valid account type") String accountType, 
		@Pattern(regexp = "[A-Z0-9]+", message = "Customer Id should contact only upper case and numbers") String customerId,@Min(1) BigDecimal balance) {

}
