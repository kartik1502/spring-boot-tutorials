package org.training.inheritance.mapping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.training.inheritance.mapping.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Long>{
	
	@Query("select count(*) from Account")
	long count();

}