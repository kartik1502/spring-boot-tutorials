package org.training.inheritance.mapping.dto;

import java.math.BigDecimal;

public record ViewAccount(String accountNumber, String accounType, BigDecimal balance
		,ViewCustomer viewCustomer) {
}
