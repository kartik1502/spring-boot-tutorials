package org.training.inheritance.mapping.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.inheritance.mapping.dto.Register;
import org.training.inheritance.mapping.dto.Response;
import org.training.inheritance.mapping.service.CustomerService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
public class RegistrationController {

	private final CustomerService customerService;
	
	@PostMapping
	public ResponseEntity<Response> registerCustomer(@RequestBody @Valid Register register) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(customerService.register(register));
	}
	
}
