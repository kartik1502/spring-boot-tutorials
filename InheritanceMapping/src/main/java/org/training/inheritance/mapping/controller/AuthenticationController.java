package org.training.inheritance.mapping.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.training.inheritance.mapping.dto.Response;
import org.training.inheritance.mapping.service.CustomerService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthenticationController {

	private final CustomerService customerService;
	
	@PutMapping("/login")
	public ResponseEntity<Response> login(@RequestParam String customerId, @RequestParam String password) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(customerService.login(customerId, password));
	}
	
	@PutMapping("/logout")
	public ResponseEntity<Response> logout(@RequestParam String customerId) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(customerService.logout(customerId));
	}
}
