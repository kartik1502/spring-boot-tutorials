package org.training.bank.application.service.implementation;

import java.util.List;

import org.springframework.stereotype.Service;
import org.training.bank.application.dto.BeneficiaryDto;
import org.training.bank.application.entity.Account;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.exception.ResourceNotFound;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.repository.AccountRepository;
import org.training.bank.application.repository.BeneficiaryAccountRepository;
import org.training.bank.application.repository.CustomerRepository;
import org.training.bank.application.service.BeneficiaryService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BeneficiaryServiceImpl implements BeneficiaryService{
	private final BeneficiaryAccountRepository beneficiaryAccountRepository;
	private final AccountRepository accountRepository;
	private final CustomerRepository customerRepository; 
	

	@Override
	public List<BeneficiaryDto> viewBeneficiary(String accountId) {

		Account account=accountRepository.findById(accountId).orElseThrow(() -> 
				new ResourceNotFound("Account Not Found"));
		
		Customer customer=customerRepository.findById(account.getCustomer().getCustomerId())
				.orElseThrow(() -> 
				new ResourceNotFound("Customer Not Found"));
		
		if(customer.getLoginStatus().equals(LoginStatus.LOGOUT))
		{
			throw new UnAuthorizedAccess("Login to view the beneficiaries");
		}
		List<BeneficiaryDto> beneficiaryAccount=beneficiaryAccountRepository.findByAccount(account).stream() 
				.map(accounts -> 
			 new BeneficiaryDto(accounts.getDestinationAccountNumber(), accounts.getBeneficiaryName())
		).toList();
		
		
		return beneficiaryAccount ;
	}
	
	
	

}
