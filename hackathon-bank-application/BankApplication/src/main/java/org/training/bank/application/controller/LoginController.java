package org.training.bank.application.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.bank.application.dto.LoginDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.service.CustomerService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class LoginController {
	 
	private final CustomerService customerService;
	
	
	@PostMapping("/login")
	public ResponseEntity<ResponseDto> customerLogin(@Valid @RequestBody LoginDto loginDto) {
		return new ResponseEntity<>(customerService.login(loginDto), HttpStatus.CREATED);
	}

}
