package org.training.bank.application.service;

import java.util.List;

import org.training.bank.application.dto.BeneficiaryDto;

public interface BeneficiaryService {

	List<BeneficiaryDto> viewBeneficiary(String accountId);

}
