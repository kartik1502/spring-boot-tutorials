package org.training.bank.application.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GlobalException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	private String message;
	
	private String errorCode;

}
