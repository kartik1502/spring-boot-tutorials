package org.training.bank.application.dto;

public record ValidateTransfer(String referenceId, String otpToken, String customerId) {

}
