package org.training.bank.application.service;

import java.util.List;

import org.training.bank.application.dto.AccountDto;

public interface AccountService {
	
	List<AccountDto> getAccount(String customerId);

}
