package org.training.bank.application.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public record AccountDetails(String accountNumber, BigDecimal balance, LocalDate creationDate,
		java.util.List<Transactions> transactions) {

}