package org.training.bank.application.exception;

public class InvalidOperation extends GlobalException{

	private static final long serialVersionUID = 1L;

	public InvalidOperation(String message) {
		super(message, GlobalErrorCode.BAD_REQUEST);
	}

	public InvalidOperation() {
		super();
	}
	

}