package org.training.bank.application.exception;

public class GlobalErrorCode {
	
	private GlobalErrorCode () {}
	
	public static final String NOT_FOUND = "404";
	
	public static final String UN_AUTHORIZED = "401";

	public static final String BAD_REQUEST="400";


}
