package org.training.bank.application.exception;

public class InSufficientFund extends GlobalException{

	private static final long serialVersionUID = 1L;

	public InSufficientFund(String message) {
		super(message, GlobalErrorCode.BAD_REQUEST);
	}
	
	public InSufficientFund() {
		super("Insufficient funds", GlobalErrorCode.BAD_REQUEST);
	}
}
