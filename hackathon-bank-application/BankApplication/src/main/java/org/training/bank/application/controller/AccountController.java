package org.training.bank.application.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.bank.application.dto.AccountDto;
import org.training.bank.application.service.AccountService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class AccountController {
	
private final AccountService accountService;
	
	
	@GetMapping("/accounts/{customerId}")
	public ResponseEntity<List<AccountDto>> accountSummary(@Valid @PathVariable String customerId) {
		return new ResponseEntity<>(accountService.getAccount(customerId), HttpStatus.CREATED);
	}

}
