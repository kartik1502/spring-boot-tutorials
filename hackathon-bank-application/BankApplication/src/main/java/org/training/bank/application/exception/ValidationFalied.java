package org.training.bank.application.exception;

public class ValidationFalied extends GlobalException{
	

	private static final long serialVersionUID = 1L;

	public ValidationFalied(String message) {
		super(message, GlobalErrorCode.BAD_REQUEST);
	}
	
	public ValidationFalied() {
		super("Invalid Validation", GlobalErrorCode.BAD_REQUEST);
	}

}
