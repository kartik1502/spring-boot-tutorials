package org.training.bank.application.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.bank.application.entity.Account;
import org.training.bank.application.entity.Customer;

public interface AccountRepository extends JpaRepository<Account, String> {
	
	Optional<Account> findByAccountNumber(String accountNumber);
	
	List<Account> findByCustomer(Customer customer);

	List<Account> findAccountByCustomer(Customer customer);

}