package org.training.bank.application.dto;

import lombok.Builder;

@Builder
public record BeneficiaryDto(
		String destinationAccountNumber,
		String beneficiaryName
		) {

}
