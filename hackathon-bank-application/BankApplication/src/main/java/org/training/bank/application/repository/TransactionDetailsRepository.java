package org.training.bank.application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.bank.application.entity.TransactionDetails;

public interface TransactionDetailsRepository extends JpaRepository<TransactionDetails, Long> {
	
	List<TransactionDetails> findFirst10ByReferenceIdIn(List<String> referenceIds);

}
