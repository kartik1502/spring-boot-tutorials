package org.training.bank.application.service.implementation;

import java.util.List;

import org.springframework.stereotype.Service;
import org.training.bank.application.dto.AccountDto;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.exception.ResourceNotFound;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.repository.AccountRepository;
import org.training.bank.application.repository.CustomerRepository;
import org.training.bank.application.service.AccountService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService{
	
	private final CustomerRepository customerRepository;
	
	private final AccountRepository accountRepository;
	 
	
	public List<AccountDto> getAccount(String customerId)
	{
		Customer customer=customerRepository.findById(customerId).orElseThrow(() -> new ResourceNotFound("Customer Not Found"));
		if(!customer.getLoginStatus().equals(LoginStatus.LOGIN))
		{
			throw new UnAuthorizedAccess("Login is required to get the Account Summary");
		}
		List<AccountDto> accounts=accountRepository.findAccountByCustomer(customer).stream()
				.map(account -> 
					 new AccountDto(customer.getName(), 
							account.getAccountNumber(), account.getBalance(), account.getAccountType().toString())
				).toList();
		
		
		log.info("List of accounts retrieved");
		return accounts;
		
	}

}
