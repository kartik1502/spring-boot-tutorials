package org.training.bank.application.exception;


public class UnAuthorizedAccess extends GlobalException{

	private static final long serialVersionUID = 1L;

	public UnAuthorizedAccess(String message) {
		super(message, GlobalErrorCode.UN_AUTHORIZED);
	}

	public UnAuthorizedAccess() {
		super("Unautorized Access", GlobalErrorCode.UN_AUTHORIZED);
	}

	
}
