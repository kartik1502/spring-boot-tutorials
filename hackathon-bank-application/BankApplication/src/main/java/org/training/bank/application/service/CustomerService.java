package org.training.bank.application.service;

import org.training.bank.application.dto.LoginDto;
import org.training.bank.application.dto.ResponseDto;

public interface CustomerService {
	
	ResponseDto login(LoginDto loginDto);

}
