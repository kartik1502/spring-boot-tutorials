package org.training.bank.application.dto;

import java.math.BigDecimal;

import jakarta.validation.constraints.Pattern;

public record FundTransferRequest(String customerId,
		String fromAccount,
		String toAccount,
		BigDecimal amount,
		@Pattern(regexp = "(DEBIT)|(CREDIT)", message = "Transfer type can be either DEBIT or CREDIT") String transferType) {

}
