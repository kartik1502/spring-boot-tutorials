package org.training.bank.application.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.bank.application.dto.FundTransferRequest;
import org.training.bank.application.dto.OtpRequest;
import org.training.bank.application.dto.OtpResponse;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.entity.Account;
import org.training.bank.application.entity.BeneficiaryAccount;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.exception.InSufficientFund;
import org.training.bank.application.exception.ResourceNotFound;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.feign.client.OtpService;
import org.training.bank.application.repository.AccountRepository;
import org.training.bank.application.repository.BeneficiaryAccountRepository;
import org.training.bank.application.repository.CustomerRepository;
import org.training.bank.application.repository.FundTransferRepository;

@ExtendWith(SpringExtension.class)
class FundTransferServiceImplTest {

	@InjectMocks
	private FundTransferServiceImpl fundTransferService;

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private FundTransferRepository fundTransferRepository;

	@Mock
	private AccountRepository accountRepository;

	@Mock
	private BeneficiaryAccountRepository beneficiaryAccountRepository;

	@Mock
	private OtpService otpService;

	@Test
	void testInitiateFundTransfer_CustomernotFound() {

		Mockito.when(customerRepository.findById(Mockito.anyString())).thenReturn(Optional.empty());

		FundTransferRequest fundTransferRequest = new FundTransferRequest(null, null, null, null, null);
		ResourceNotFound exception = assertThrows(ResourceNotFound.class,
				() -> fundTransferService.initiateTransfer(fundTransferRequest));

		assertEquals("Customer not found", exception.getMessage());
	}

	@Test
	void testinitiateFundTransfer_CustomerNotLoggedIn() {

		Customer customer = Customer.builder().customerId("1").loginStatus(LoginStatus.LOGOUT).build();
		Mockito.when(customerRepository.findById(Mockito.anyString())).thenReturn(Optional.of(customer));

		FundTransferRequest fundTransferRequest = new FundTransferRequest("1", null, null, null, null);
		UnAuthorizedAccess exception = assertThrows(UnAuthorizedAccess.class,
				() -> fundTransferService.initiateTransfer(fundTransferRequest));

		assertEquals("Need to login, to use the bank applicaiton", exception.getMessage());
	}

	@Test
	void testInitiateFundTransfer_AccountNotFound() {

		Customer customer = Customer.builder().customerId("1").loginStatus(LoginStatus.LOGIN).build();
		Mockito.when(customerRepository.findById(Mockito.anyString())).thenReturn(Optional.of(customer));

		Mockito.when(accountRepository.findByAccountNumber(Mockito.anyString())).thenReturn(Optional.empty());
		FundTransferRequest fundTransferRequest = new FundTransferRequest("1", "A004", null, null, null);
		ResourceNotFound exception = assertThrows(ResourceNotFound.class,
				() -> fundTransferService.initiateTransfer(fundTransferRequest));

		assertEquals("Requested account not found", exception.getMessage());
	}
	
	@Test
	void testInitiateFundTransfer_InSufficientFunds() {

		Customer customer = Customer.builder().customerId("1").loginStatus(LoginStatus.LOGIN).build();
		Mockito.when(customerRepository.findById(Mockito.anyString())).thenReturn(Optional.of(customer));

		Account account = Account.builder().balance(BigDecimal.valueOf(3000)).build();
		
		Mockito.when(accountRepository.findByAccountNumber(Mockito.anyString())).thenReturn(Optional.of(account));
		FundTransferRequest fundTransferRequest = new FundTransferRequest("1", "A004", null, BigDecimal.valueOf(30000), null);
		
		InSufficientFund exception = assertThrows(InSufficientFund.class,
				() -> fundTransferService.initiateTransfer(fundTransferRequest));

		assertEquals("Insufficient funds", exception.getMessage());
	}
	
	@Test
	void testInitiateFundTransfer_BeneficiaryNotFound() {

		Customer customer = Customer.builder().customerId("1").loginStatus(LoginStatus.LOGIN).build();
		Mockito.when(customerRepository.findById(Mockito.anyString())).thenReturn(Optional.of(customer));

		Account account = Account.builder().balance(BigDecimal.valueOf(3000)).build();
		
		Mockito.when(accountRepository.findByAccountNumber(Mockito.anyString())).thenReturn(Optional.of(account));
		FundTransferRequest fundTransferRequest = new FundTransferRequest("1", "A004", null, BigDecimal.valueOf(300), null);
		
		Mockito.when(beneficiaryAccountRepository.findByDestinationAccountNumber(Mockito.anyString())).thenReturn(Optional.empty());
		
		ResourceNotFound exception = assertThrows(ResourceNotFound.class,
				() -> fundTransferService.initiateTransfer(fundTransferRequest));

		assertEquals("Requested destination account not found", exception.getMessage());
	}
	
	@Test
	void testInitiateFundTransfer_Success() {

		Customer customer = Customer.builder().customerId("1").loginStatus(LoginStatus.LOGIN).build();
		Mockito.when(customerRepository.findById(Mockito.anyString())).thenReturn(Optional.of(customer));

		Account account = Account.builder().balance(BigDecimal.valueOf(3000)).build();
		
		Mockito.when(accountRepository.findByAccountNumber(Mockito.anyString())).thenReturn(Optional.of(account));
		FundTransferRequest fundTransferRequest = new FundTransferRequest("1", "A004", "BA009", BigDecimal.valueOf(300), "DEBIT");
		
		BeneficiaryAccount beneficiaryAccount = BeneficiaryAccount.builder()
				.account(account)
				.destinationAccountNumber("BA009").build();
		
		Mockito.when(otpService.getOtpToken(Mockito.any(OtpRequest.class))).thenReturn(new OtpResponse("889009"));
		
		Mockito.when(beneficiaryAccountRepository.findByDestinationAccountNumber(Mockito.anyString())).thenReturn(Optional.of(beneficiaryAccount));
		
		ResponseDto response = fundTransferService.initiateTransfer(fundTransferRequest);
		assertNotNull(response);
	}
	
	
}
