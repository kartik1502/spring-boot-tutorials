package org.training.bank.application.entity;

import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OtpValidation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long validationId;
	
	@ManyToOne
	@JoinColumn(name = "customerId")
	private Customer customer;
	
	private String otpToken;
	
	private LocalDateTime generationTime;
	
	private LocalDateTime expiryTime;
}