package org.training.bank.application.dto;

import java.math.BigDecimal;

public record ViewAccount(String accountNumber, String accounType, BigDecimal balance
		,ViewCustomer viewCustomer) {
}