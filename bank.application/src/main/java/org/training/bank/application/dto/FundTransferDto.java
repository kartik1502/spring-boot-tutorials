package org.training.bank.application.dto;

import java.math.BigDecimal;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FundTransferDto {

	@NotEmpty
	private String fromAccountNumber;
	
	@NotEmpty
	private String toAccountNumber;
	
	@Min(1)
	private BigDecimal amount;
	
	@NotEmpty
	private String accountType;
}
