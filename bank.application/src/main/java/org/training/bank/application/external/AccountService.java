package org.training.bank.application.external;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.training.bank.application.configuration.FeignConfiguration;
import org.training.bank.application.dto.ViewAccount;

@FeignClient(value = "account-service", url = "http://localhost:9000/accounts", configuration = FeignConfiguration.class)
public interface AccountService {
	
	@GetMapping
	ViewAccount getAccount(@RequestParam String customerId);

}
