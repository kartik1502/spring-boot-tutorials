package org.training.bank.application.exception;

public class PasswordMatchException extends RuntimeException{
	private static final long serialVersionUID = 1L;

    public PasswordMatchException () {
        super("Password doesn't match");
    }

    public PasswordMatchException  (String message) {
        super(message);
    }

}
