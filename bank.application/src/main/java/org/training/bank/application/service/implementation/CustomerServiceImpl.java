package org.training.bank.application.service.implementation;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.training.bank.application.dto.AccountDto;
import org.training.bank.application.dto.LoginDto;
import org.training.bank.application.dto.RegisterDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.entity.Account;
import org.training.bank.application.entity.AccountType;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.Generator;
import org.training.bank.application.entity.IfscCode;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.exception.PasswordMatchException;
import org.training.bank.application.exception.ResourceConflictExists;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.repository.AccountRepository;
import org.training.bank.application.repository.CustomerRepository;
import org.training.bank.application.service.CustomerService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService{

	private final CustomerRepository customerRepository;
	
	private final AccountRepository accountRepository;
	
	@Value("${spring.application.unauthorized.access}")
	private String unAuthorizedAccess;
	
	@Value("${spring.application.ok}")
	private String responseOk;

	@Override
	public AccountDto login(LoginDto loginDto) {
		
		Customer customer = customerRepository.findByCustomerIdAndPassword(loginDto.getCustomerId(), loginDto.getPassword())
				.orElseThrow(() -> {
					log.warn(unAuthorizedAccess);
					return new UnAuthorizedAccess(unAuthorizedAccess);
				});
		
		customer.setLoginStatus(LoginStatus.LOGGEDIN);
		customerRepository.save(customer);
		log.info("Logged In Successfully");
		
		AccountDto accountDto = new AccountDto();
		BeanUtils.copyProperties(customer.getAccount(), accountDto);
		accountDto.setAccountType(customer.getAccount().getAccountType().toString());
		accountDto.setIfscCode(customer.getAccount().getIfscCode().toString());
		return accountDto;
	}

	@Override
	public ResponseDto register(@Valid RegisterDto registerDto) {
		
		if(!registerDto.getPassword().equals(registerDto.getConfirmPassword())) {
			log.warn("password and confirm need to be the same");
			throw new PasswordMatchException("Password doesn't match");
		}
		
		Optional<Customer> customer = customerRepository.findByEmailIdOrContactNo(registerDto.getEmailId(),registerDto.getContactNo());
		if (customer.isPresent()) {
			log.warn("Customer already registerd");
			throw new ResourceConflictExists("User already Registered");
		}
		String customerId = Generator.UTIB.toString() + String.format("%06d", customerRepository.count());
		Customer newCustomer = Customer.builder().emailId(registerDto.getEmailId()).password(registerDto.getPassword())
				.name(registerDto.getName()).loginStatus(LoginStatus.LOGGEDOUT).customerId(customerId).aadhar(registerDto.getAadhar()).contactNo(registerDto.getContactNo()).build();
		
		
		String accountNumber = Generator.AUKB.toString() + String.format("%07d", accountRepository.count());
		
		Account account = Account.builder()
				.accountNumber(accountNumber)
				.balance(BigDecimal.valueOf(5000))
				.ifscCode(IfscCode.UTIB098)
				.accountType(AccountType.SAVINGS)
				.build();
		
		newCustomer.setAccount(account);
		log.info("Customer registered successfully");
		customerRepository.save(newCustomer);
		accountRepository.save(account);
		log.info("Account is created, for the customer");
		return ResponseDto.builder().responseCode(responseOk)
				.responseMessage("Customer Registered sucessfully").build();
	}
}
