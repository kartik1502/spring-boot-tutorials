package org.training.bank.application.service;

import org.training.bank.application.dto.FundTransferDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.dto.ValidateDto;
import org.training.bank.application.entity.OtpValidation;

import jakarta.validation.Valid;

public interface FundTransferService {

	ResponseDto fundTransfer(@Valid FundTransferDto fundTransferDto);

	ResponseDto validateTransfer(ValidateDto otpValidation);

}
