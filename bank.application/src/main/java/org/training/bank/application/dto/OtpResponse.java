package org.training.bank.application.dto;

import lombok.Data;

@Data
public class OtpResponse {
	
	private String otp;

}
