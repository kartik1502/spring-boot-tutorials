package org.training.bank.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.bank.application.entity.OtpValidation;

public interface OtpValidationRepository extends JpaRepository<OtpValidation, Long> {

}