package org.training.bank.application.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Request {
	
	private String to;

}
