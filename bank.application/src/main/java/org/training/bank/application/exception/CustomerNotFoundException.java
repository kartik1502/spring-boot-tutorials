package org.training.bank.application.exception;

public class CustomerNotFoundException extends RuntimeException{
	private static final long serialVersionUID = 1L;

    public CustomerNotFoundException () {
        super("Password doesn't match");
    }

    public CustomerNotFoundException  (String message) {
        super(message);
    }
}
