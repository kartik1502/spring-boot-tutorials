package org.training.bank.application.service.implementation;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.training.bank.application.dto.FundTransferDto;
import org.training.bank.application.dto.OtpResponse;
import org.training.bank.application.dto.Request;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.dto.ValidateDto;
import org.training.bank.application.entity.Account;
import org.training.bank.application.entity.BeneficiaryAccount;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.FundTransfer;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.entity.OtpValidation;
import org.training.bank.application.entity.TransferStatus;
import org.training.bank.application.entity.TransferType;
import org.training.bank.application.exception.InSufficientFunds;
import org.training.bank.application.exception.ResourceNotFound;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.external.OtpService;
import org.training.bank.application.repository.AccountRepository;
import org.training.bank.application.repository.BeneficiaryAccountRepository;
import org.training.bank.application.repository.CustomerRepository;
import org.training.bank.application.repository.FundTransferRepository;
import org.training.bank.application.service.FundTransferService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class FundTransferServiceImpl implements FundTransferService {

	private final CustomerRepository customerRepository;

	private final AccountRepository accountRepository;
	
	private final BeneficiaryAccountRepository beneficiaryAccountRepository;
	
	private final FundTransferRepository fundTransferRepository;
	
	private final OtpService otpService;
	
	@Value("${spring.application.ok}")
	private String responseOk;

	@Override
	public ResponseDto fundTransfer(@Valid FundTransferDto fundTransferDto) {

		Account fromAccount = accountRepository.findAccountByAccountNumber(fundTransferDto.getFromAccountNumber())

				.orElseThrow(() -> {
					log.warn("Requested Account not found");
					return new ResourceNotFound("Requested from Account not found");
				});
		
		Customer customer = customerRepository.findByAccount(fromAccount)
				.orElseThrow(() -> {
					log.warn("Customer not found");
					return new ResourceNotFound("Requested Customer not found");
				});
		
		if(customer.getLoginStatus().equals(LoginStatus.LOGGEDOUT)) {
			log.warn("Unauthorized access, login to make fund transfers");
			throw new UnAuthorizedAccess("Unauthorized: Login to make fund transfers");
		}

		BeneficiaryAccount toAccount = beneficiaryAccountRepository.findByAccountNumber(fundTransferDto.getToAccountNumber())

				.orElseThrow(() -> {
					log.warn("Requested Account not found");
					return new ResourceNotFound("Requested to Account not found");
				});
		
		log.info("Account checks done, waiting for the transfer");
		
		
		FundTransfer fundTransfer = FundTransfer.builder()
				.referenceId(UUID.randomUUID().toString())
				.fromAccount(fromAccount.getAccountNumber())
				.toAccount(toAccount.getAccountNumber())
				.amount(fundTransferDto.getAmount())
				.transferType(TransferType.valueOf(fundTransferDto.getAccountType()))
				.status(TransferStatus.PENDING)
				.otpToken(otpService.getOtp(Request.builder().to("karthikbanu.kulkarni@hcl.com").build()).getOtp())
				.build();
		
		fundTransferRepository.save(fundTransfer);
		log.info("Fund transfer initiated");
		return ResponseDto.builder().responseCode(responseOk)
				.responseMessage("Fund transfer initiated, validate transter").build();
	}

	@Override
	public ResponseDto validateTransfer(ValidateDto otpValidation) {
		
		FundTransfer fundTransfer = fundTransferRepository.findByReferenceId(otpValidation.getReferenceId())
				.orElseThrow(() -> {
					log.warn("No such fund transfer");
					return new ResourceNotFound("Fund transfer not found");
				});
		
		fundTransfer.setStatus(TransferStatus.SUCCESS);
		
		Account account = accountRepository.findAccountByAccountNumber(fundTransfer.getFromAccount())
				.orElseThrow(() -> {
					log.warn("Account not found");
					return new ResourceNotFound("Requested account found");
				});
		
		BeneficiaryAccount beneficiaryAccount = beneficiaryAccountRepository.findByAccountNumber(fundTransfer.getToAccount())
				.orElseThrow(() -> {
					log.warn("Beneficiary account not found");
					return new ResourceNotFound("Requested beneficiary account found");
				});
		
		if (account.getBalance().compareTo(fundTransfer.getAmount()) < 0) {
			log.warn("InSufficent funds in the account");
			throw new InSufficientFunds("InSufficent funds in the account");
		}
		
		if(!fundTransfer.getOtpToken().equals(otpValidation.getOtp())) {
			
			throw new UnAuthorizedAccess("Invalid otp");
		}
		
		account.setBalance(account.getBalance().subtract(fundTransfer.getAmount()));
		beneficiaryAccount.setBalance(beneficiaryAccount.getBalance().add(fundTransfer.getAmount()));
		
		accountRepository.save(account);
		fundTransferRepository.save(fundTransfer);
		beneficiaryAccountRepository.save(beneficiaryAccount);
		return ResponseDto.builder().responseCode(responseOk)
				.responseMessage("Fund transfer successfull").build();
	}

}
