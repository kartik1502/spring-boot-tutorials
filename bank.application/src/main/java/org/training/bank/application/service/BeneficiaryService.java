package org.training.bank.application.service;

import org.training.bank.application.dto.BeneficiaryDto;
import org.training.bank.application.dto.ResponseDto;

public interface BeneficiaryService {

	ResponseDto beneficiary(BeneficiaryDto beneficiaryDto);
}
