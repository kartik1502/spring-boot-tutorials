package org.training.bank.application.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.training.bank.application.dto.FundTransferDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.dto.ValidateDto;
import org.training.bank.application.dto.ViewAccount;
import org.training.bank.application.entity.OtpValidation;
import org.training.bank.application.external.AccountService;
import org.training.bank.application.service.FundTransferService;


import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/fund-transfers")
@RequiredArgsConstructor
public class FundTransferController {

	private final FundTransferService fundTransferService;
	
	private final AccountService accountService;
	
	@PostMapping
	public ResponseEntity<ResponseDto> fundTransfer(@RequestBody @Valid FundTransferDto fundTransferDto) {
		log.info("Intiating the fund transfer....");
		return new ResponseEntity<>(fundTransferService.fundTransfer(fundTransferDto), HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<ResponseDto> validateTransfer(@RequestBody ValidateDto otpValidation) {
		return new ResponseEntity<>(fundTransferService.validateTransfer(otpValidation), HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<ViewAccount> getAccount(@RequestParam String customerId) {
		return ResponseEntity.ok()
				.body(accountService.getAccount(customerId));
	}
}
