package org.training.bank.application.exception;

public class InSufficientFunds extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public InSufficientFunds(String message) {
		super(message);
	}

	public InSufficientFunds() {
		super();
	}
	
	

}
