package org.training.bank.application.dto;

public record ViewCustomer(String customerId, String name, String aadhar, String emailId,
		String contactNo) {

}
