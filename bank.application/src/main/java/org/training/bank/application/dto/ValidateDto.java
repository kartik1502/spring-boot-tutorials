package org.training.bank.application.dto;

import lombok.Data;

@Data
public class ValidateDto {
	
	private String referenceId;
	
	private String otp;

}
