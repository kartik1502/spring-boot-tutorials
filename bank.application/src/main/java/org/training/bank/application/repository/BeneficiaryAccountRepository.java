package org.training.bank.application.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.bank.application.entity.BeneficiaryAccount;

public interface BeneficiaryAccountRepository extends JpaRepository<BeneficiaryAccount, Long> {

	Optional<BeneficiaryAccount> findByAccountNumber(String accountNumber);
	
	Optional<BeneficiaryAccount> findByCustomerId(long customerId);
}
