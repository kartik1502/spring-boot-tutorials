package org.training.bank.application.entity;

public enum AccountType {
	
	SAVINGS, CURRENT, LOAN

}
