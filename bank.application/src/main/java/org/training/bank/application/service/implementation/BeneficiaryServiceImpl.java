package org.training.bank.application.service.implementation;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.training.bank.application.dto.BeneficiaryDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.entity.BeneficiaryAccount;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.exception.ResourceNotFound;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.repository.BeneficiaryAccountRepository;
import org.training.bank.application.repository.CustomerRepository;
import org.training.bank.application.service.BeneficiaryService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class BeneficiaryServiceImpl implements BeneficiaryService {
	
	private final CustomerRepository customerRepository;
	
	private final BeneficiaryAccountRepository beneficiaryAccountRepository;

	@Override
	public ResponseDto beneficiary(BeneficiaryDto beneficiaryDto)
	{
		Customer customer=customerRepository.findByCustomerId(beneficiaryDto.getCustomerId())
				.orElseThrow(() -> {
					
					log.warn("Please register, customer not found");
					return new ResourceNotFound("Customer not Found Please Register");
				});
		
		if(customer.getLoginStatus().equals(LoginStatus.LOGGEDOUT)) {
			log.warn("Unauthorized access, login to make fund transfers");
			throw new UnAuthorizedAccess("Unauthorized: Login to make fund transfers");
		}
		BeneficiaryAccount beneficiaryAccount=BeneficiaryAccount.builder().accountNumber(beneficiaryDto.getAccountNumber()).accountHolderName(beneficiaryDto.getAccountHolderName()).balance(beneficiaryDto.getBalance()).branchName(beneficiaryDto.getBranchName()).ifscCode(beneficiaryDto.getIfscCode()).build();
		beneficiaryAccount.setCustomer(customer);
		beneficiaryAccountRepository.save(beneficiaryAccount);
		log.info("Beneficiary account added successfully");
		return ResponseDto.builder().responseCode("201")
				.responseMessage("Beneficiary Account created").build();

	}
}
