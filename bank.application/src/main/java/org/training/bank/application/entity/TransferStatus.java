package org.training.bank.application.entity;

public enum TransferStatus {
	PENDING, FAILED, SUCCESS

}
