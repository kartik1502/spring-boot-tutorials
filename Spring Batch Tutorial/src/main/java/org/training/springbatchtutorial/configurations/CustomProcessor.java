package org.training.springbatchtutorial.configurations;

import org.springframework.batch.item.ItemProcessor;
import org.training.springbatchtutorial.exception.InvalidInputException;
import org.training.springbatchtutorial.model.entity.Employee;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomProcessor implements ItemProcessor<Employee, Employee> {

    @Override
    public Employee process(Employee employee) throws Exception {
    	if(employee.getFirstName().isEmpty()) {
    		throw new InvalidInputException("First name not found");
    	}
        return employee;	
    }

}
