package org.training.springbatchtutorial.configurations;

import org.springframework.batch.core.SkipListener;
import org.training.springbatchtutorial.model.entity.Employee;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StepSkipListner implements SkipListener<Employee, Number>{
	
	@Override
	public void onSkipInProcess(Employee item, Throwable t) {
		
		log.info("The record: {} is skipped due to: {}", item.toString(), t.getLocalizedMessage());
	}
	

}
