package org.training.merge.conflict.exception;

public class GlobalErrorCode {
	
	private GlobalErrorCode() {}
	
	public static final String NOT_FOUND = "404";
	
	public static final String BAD_REQUEST = "400";

}
