package org.training.merge.conflict.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GlobalException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private final String errorMessage;
	
	private final String errorCode;
}
