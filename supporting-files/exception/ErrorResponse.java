package org.training.merge.conflict.exception;

import java.util.List;

public record ErrorResponse(String errorCode, List<String> errors) {

}
