package org.training.merge.conflict.exception;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.ErrorResponseException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {

		List<String> errors = ex.getBindingResult().getAllErrors().stream()
				.map(ObjectError::getDefaultMessage).toList();
				
		return new ResponseEntity<>(new ErrorResponse(GlobalErrorCode.BAD_REQUEST, errors), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(GlobalException.class)
	public ResponseEntity<Object> handleGlobalException(GlobalException globalException) {
		return ResponseEntity.status(HttpStatus.valueOf(globalException.getErrorCode()))
				.body(new ErrorResponse(globalException.getErrorCode(), 
						List.of(globalException.getLocalizedMessage())));
	}

}
