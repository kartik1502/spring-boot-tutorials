package com.training.export.csv.service;

import java.util.List;

import com.training.export.csv.entity.Employee;

public interface EmployeeService {

	List<Employee> getAll();

}
