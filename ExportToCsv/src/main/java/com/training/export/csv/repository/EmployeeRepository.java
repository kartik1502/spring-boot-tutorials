package com.training.export.csv.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.export.csv.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, String>{

}
