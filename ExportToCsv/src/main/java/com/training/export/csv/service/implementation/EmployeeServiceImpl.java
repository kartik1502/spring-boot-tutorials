package com.training.export.csv.service.implementation;

import java.util.List;

import org.springframework.stereotype.Service;

import com.training.export.csv.entity.Employee;
import com.training.export.csv.repository.EmployeeRepository;
import com.training.export.csv.service.EmployeeService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

	private final EmployeeRepository repository;
	
	@Override
	public List<Employee> getAll() {
		
		return repository.findAll();
	}

}
