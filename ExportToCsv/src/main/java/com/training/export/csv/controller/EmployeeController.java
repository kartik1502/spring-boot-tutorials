package com.training.export.csv.controller;

import java.io.IOException;

import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.ICSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.training.export.csv.entity.Employee;
import com.training.export.csv.service.EmployeeService;

import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class EmployeeController {

	private final EmployeeService employeeService;
	
	@GetMapping("/export")
	public void getData(HttpServletResponse response) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
		
		String fileName = "employee-export.csv";
		
		response.setContentType("text/csv");
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+fileName + "\"");
		
		
		StatefulBeanToCsv<Employee> writer = 
	            new StatefulBeanToCsvBuilder<Employee>
	                 (response.getWriter())
	                 .withQuotechar(ICSVWriter.NO_QUOTE_CHARACTER)
	                   .withSeparator(ICSVWriter.DEFAULT_SEPARATOR)
	            .withOrderedResults(false).build();
		
		writer.write(employeeService.getAll());

		 
		
	}
}
